from django.db import models


# Create your models here.
class AuthToken(models.Model):
    email = models.TextField(null=False, blank=False, max_length=64)
    username = models.TextField(null=False, blank=False, max_length=16)
    expiration = models.IntegerField(null=False, blank=False)
