from datetime import datetime


def str_to_datetime(data: str) -> datetime:
    return datetime.strptime(data, '%Y-%m-%d %H:%M:%S.%f')


def datetime_to_str(data: datetime) -> str:
    return datetime.strftime(data, '%Y-%m-%d %H:%M:%S.%f')


def datetime_to_int(data: datetime) -> int:
    return int(datetime.strftime(data, '%Y%m%d%H%M%S'))


def int_to_datetime(data: int) -> datetime:
    return datetime.strptime(str(data), '%Y%m%d%H%M%S')
