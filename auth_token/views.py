from datetime import datetime, timedelta

import jwt
from django.contrib.auth.models import User
from django.db.models import QuerySet
from django.http import JsonResponse

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from auth_token import token
from auth_token.datetime_conv import datetime_to_int
from auth_token.models import AuthToken


@csrf_exempt
def authenticate(request):
    context = {'code': 200}

    try:
        email = request.POST['email']
        password: str = request.POST['password']
    except KeyError:
        context = {'code': 400, 'data': 'empty email or password'}
        pass

    # Fetch account from database
    if context['code'] == 200:
        try:
            user: QuerySet = User.objects.filter(email__exact=email, password__exact=password)
            context = {'code': 200, 'data': token.create_token({'email': email, 'user': 'verified one'})}
        except User.DoesNotExist:
            context = {'code': 400, 'data': 'invalid email and password'}

        return JsonResponse(context, json_dumps_params={'ensure_ascii': False})


@csrf_exempt
def validate(request):
    context = {'code': 200}
    auth_token: str = ''

    try:
        auth_token = request.POST['token']
    except KeyError:
        context = {'code': 400, 'data': 'empty token'}

    if context['code'] == 200:
        try:
            payload: AuthToken = token.validate_token(auth_token)
            print(payload)

            if payload['expiration'] < datetime_to_int(datetime.now()):
                raise jwt.ExpiredSignatureError
            context['data'] = payload

        except jwt.DecodeError as e:
            context = {'code': 400, 'data': 'wrong token'}
        except jwt.ExpiredSignatureError:
            context = {'code': 400, 'data': 'expired token'}

    return JsonResponse(context, json_dumps_params={'ensure_ascii': False})
