from datetime import datetime, timedelta

import jwt

from auth_token.datetime_conv import datetime_to_str, datetime_to_int
from auth_token.models import AuthToken
from auth_token.settings import SECRET, ALG


def create_token(data: object) -> str:
    hour_after = datetime.now() + timedelta(hours=1)
    data['expiration'] = datetime_to_int(hour_after)
    token_bytes = jwt.encode(data, SECRET, ALG)
    return token_bytes.decode('utf-8')


def validate_token(token: str) -> AuthToken:
    return jwt.decode(token, SECRET, ALG)

