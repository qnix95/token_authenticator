from django.urls import path

from auth_token import views

urlpatterns = [
    path('signin', views.authenticate, name='authenticate_token'),
    path('validate', views.validate, name='validate_token'),
]